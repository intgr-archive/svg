Unless otherwise specified, permission is granted to use any image in any way
seen fit.

bottleneck.svg
--------------
Dual-licensed under either:
  (a) GNU GFDL 1.2 or later,
  (b) Creative Commons Attribution ShareAlike 2.5, 2.0 and 1.0

Original author: Smurrayinchester
(http://commons.wikimedia.org/wiki/User:Smurrayinchester)

